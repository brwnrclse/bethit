#!/usr/bin/env node
const
  favicons = require(`favicons`),
  fs = require(`fs`),

  faviconSrc = `assets/img/favicon.png`,
  faviconCfg = {
    appName: `varden`,
    appDescription: `The home page of brwnrclse`,
    developerName: `brwnrclse (Barry Harris)`,
    developerURL: `https://brwnrclse.xyz`,
    path: `/`,
    url: `https://bti.brwnrclse.xyz`,
    display: `standalone`,
    orientation: `portrait`,
    version: `1.0`,
    logging: false,
    online: false,
    icons: {
      android: false,
      appleIcon: true,
      appleStartup: false,
      coast: false,
      favicons: true,
      firefox: false,
      opengraph: false,
      twitter: false,
      windows: false,
      yandex: false
    }
  },
  faviconCb = (err, res) => {
    if (err) {
      throw err;
    }

    res.images.map((image) => {
      fs.writeFileSync(`./public/assets/favicon/${image.name}`,
        image.contents);
    });
  };

favicons(faviconSrc, faviconCfg, faviconCb);
